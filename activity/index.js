trainer = {
    name: "Ash Ketchum",
    age: 10,
    pokemons: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    friends: ["Brock", "Misty"],
    talk: function(pokemon){
        return console.log(pokemon + "! I choose you!");
    }

};

console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer["pokemons"]);
console.log(trainer.talk("Pikachu"));


function pokemon(name, level){
    this.name = name;
    this.level = level;
    this.health = level*3;
    this.attack = level*1.5;
    this.tackle = function(pokemon){
        console.log(this.name, "tackled", pokemon.name + "!");
        pokemon.health = pokemon.health - this.attack;
        console.log(pokemon.name + "'s health is now reduced to", pokemon.health + "!");
        if (pokemon.health <= 0){
            pokemon.faint();
        }
    };
    this.faint = function(){
        console.log(this.name, "has fainted!");
    };
}

pokemon1 = new pokemon("Pikachu", 12);
pokemon2 = new pokemon("Geodude", 8);
pokemon3 = new pokemon("Mewto", 100);

console.log(pokemon1);
console.log(pokemon2);
console.log(pokemon3);

pokemon2.tackle(pokemon1);
console.log(pokemon1);
pokemon3.tackle(pokemon2);
console.log(pokemon2);