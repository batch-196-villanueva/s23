console.log("Hello World!");

// JS Objects
// JS Objects is a way to define a real world object with its own characteristics.
// It is also a way to organize data with context through the use of key-value pairs.

/*
--Dog Characteristics
    loyal,
    huggable,
    4 legs,
    furry,
    different colors,
    hyperactive,
    tail,
    breed
*/

// Create a list, use an array
// let dog = ["loyal", 4, "tail", "Husky"];



// Describe something with characteristics, use object
let dog = {
    breed: "Husky",
    color: "White",
    legs: 4,
    isHuggable: true,
    isLoyal: true
};


let videoGame = {
    title: "Life is Strange",
    publisher: "Square Enix",
    year: 2015,
    price: "\u20B1865 (Steam)",
    isAvailable: true
};

console.log(videoGame);

// If [] are used to create array, what did you use to create your object?
// [] - array literals create arrays
// {} - object literals create object

// Objects are composed of key-value pairs. keys provide label to your values.
// key: value
// Each key-value pair together are called properties

// Accessing array items = arrName[index]
// Accessing Object properties = objectName.propertyName

console.log(videoGame.title);
console.log(videoGame.publisher);

// Can we also update the properties of our object?
// object.propertyName = <newValue>

videoGame.price = "\u20B1750";

videoGame.title = "Final Fantasy X";
videoGame.year = "2001"

console.log(videoGame);

// Objects can not only have primitive values like strings, numbers or boolean
// It can also contain objects and arrays

let course = {
    title: "Philosophy 101",
    description:"Learn the values of life",
    price: 5000,
    isActive: true,
    instructors: ["Mr. Johnson", "Mrs. Smith", "Mr. Francis"]
};

console.log(course);
// Can we access the instructors array?
console.log(course.instructors);
// How can we access Mrs. Smith, an instructor from instructors array?
console.log(course.instructors[1]);
// Can we use the instructor array's array methods?
course.instructors.pop();
console.log(course);

// 1. Add a new instructor
course.instructors.push("Mr. McGee");
console.log(course.instructors);

findInstructor = course.instructors.includes("Mr. Johnson");
console.log(findInstructor);

// Create a function to be able to add new instructors to our object

function addNewInstructor(name){

    if(course.instructors.includes(name)){
        console.log("Instructor already added!");
    } else {
        course.instructors.push(name);
        console.log("Thank you. Instructor added.");
    };    
};

addNewInstructor("Mr. Marco");
addNewInstructor("Mr. Smith");

console.log(course);

// We can also create/initialize an empty object and add its properties afterwards

let instructor = {};
console.log(instructor);

// If you assign a value to a property that does not yet exist, you are able to add a new property in the object

instructor.name = "James Johnson";
instructor.age = 56;
instructor.gender = "male";
instructor.department = "Humanities";
instructor.salary = "\u20B150000";
instructor.subjects = ["Philosophy", "Humanities", "Logic"];

console.log(instructor);

instructor.address = {
    street: "#1 Maginhawa St.",
    city: "Quezon City",
    country: "Philippines"
};

console.log(instructor);
console.log(instructor.address.street);
// Create Objects Using a constructor function

// Create a reusable function to create objects whose structure and keys are the same. Think of creating a function that serves a blueprint for an object.

function Superhero(name, superpower, powerLevel){
    //"this" keyword when added in a constructor function refers to the object that will be made by the function.
/*

    {
        name: <valueOfParameterName>
        superpower: <valueOfParameterSuperpower>
        powerLevel: <valueOfParameterPowerLevel>

    }


*/

    this.name = name;
    this.superpower = superpower;
    this.powerLevel = powerLevel;
    
}

// Create an object out of our Superhero constructor function
// new keyword is added to allow to create a new object out of our function.

let superhero1 = new Superhero("Saitama", "One Punch", 30000);
console.log(superhero1);

function item(name,brand,price){
    this.name = name;
    this.brand = brand;
    this.price = price;
}

laptop1 = new item("Inspiron 14", "Dell", 34000);
laptop2 = new item("Nitro 5", "Acer", 45000);

console.log(laptop1);
console.log(laptop2);

